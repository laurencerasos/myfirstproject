<!DOCTYPE html>
<html>
<head>
	<title>readmore</title>
</head>
<style>
	
	h3
	{
	color:black;
	font-family:fantasy;
	border-style:solid;
	margin-right: 200px;
	margin-left:100px;
	border-radius: 50px;
    background: gold;
    padding: 20px; 
    width: 950px;
    height: 10px;
    text-align: center;
	}
	.page
	{
	color:black;
	font-family:fantasy;
	border-style:solid;
	margin-right: 200px;
	margin-left:100px;
	border-radius: 50px;
    background: gold;
    padding: 100px; 
    width: 800px;
    height: 300px;
    text-align: center;
	}
	</style>
<body>

	<div align="middle">

		<img src="images/logo3.png" align="left"> 
		<h1 style="font-family: Old English Text MT; color: green;">St. Paul University Philippines</h1>
		<p>Tuguegarao City</p> <br>

	</div>

	<hr>
		<h3 align="center">SPUP'S HEALTH SERVICES REACHES OUT TO RHWC</h3><br>
	<hr>

<div class="page">
	<p><i>2017-09-02</i></p>
	
<br>
	<p>
		(by: Ms. Zylla Ezra Tenedero and Mr. Jaypee Talosig)
<br>
<br>
		SPUP's Health Services Unit (HSU) initiated an Outreach Activity at the Regional Haven for Women and Children (RHWC) in Maddarulug, Solana on September 2, 2017. Dr. Maricon Manuel (Head, Health Services Unit) catered to the Medical needs of the residents of the center; while, Dr. Grace Tamayao and Dr. Angelica Asuncion (University Dentists) conducted Dental services to the children at the center. Ms. Badet Quiambao, Mr. Jaypee Talosig and Ms. Zylla Ezra Tenedero (University Nurses) manned the feeding program activity. Former University Nurses, Ms. Danica Lim (now working at St. Paul Hospital) and Ms. Preiyanne Kaye Ulep (now based in New Jersey, USA) extended their help by rendering their services to the Outreach Activity. The HSU Personnel also gave simple gifts to the children as advanced-presents for the Yuletide season.
	</p>
</div>
</body>
</html>