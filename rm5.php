<!DOCTYPE html>
<html>
<head>
	<title>readmore</title>
</head>

<style>
	
	h3
	{
	color:black;
	font-family:fantasy;
	border-style:solid;
	margin-right: 200px;
	margin-left:100px;
	border-radius: 50px;
    background: gold;
    padding: 20px; 
    width: 950px;
    height: 10px;
    text-align: center;
	}
	.page
	{
	color:black;
	font-family:fantasy;
	border-style:solid;
	margin-right: 200px;
	margin-left:100px;
	border-radius: 50px;
    background: gold;
    padding: 100px; 
    width: 800px;
    height: 300px;
    text-align: center;
	}
</style>



<body>

	<div align="middle">

		<img src="images/logo3.png" align="left"> 
		<h1 style="font-family: Old English Text MT; color: green;">St. Paul University Philippines</h1>
		<p>Tuguegarao City</p> <br>

	</div>

	<hr>
		<h3 align="center">SPUP RECOGNIZES INTERNS FROM CJCU, TAIWAN</h3><br>
	<hr>
<div class="page">
	<p><i>2017-08-30</i></p>
	
<br>
	<p>
		(by: Dr. Jeremy Godofredo Morales)
<br>
<br>
		August 30, 2017 - St. Paul University Philippines (SPUP) through the Intercultural Institute for Languages (IIL) and the Graduate School, awarded Certificate of Completion and Certificate of Recognition to Taiwanese Language Interns from the Chang Jung Christian University (CJCU) namely Hu Shih-Yu, Wu Chih-Ying, and Huang Yu-Ying. These interns have completed their one-month internship program at SPUP doing translation activities by teaching Chinese as a Foreign Language to doctoral students in various fields. Sister Merceditas Ang, SPC (University President), headed the ceremony and witnessed by academic leaders. This exchange activity is for continuing implementation of the bilateral agreement of CJCU and SPUP for four years now.

	</p>
</div>
</body>
</html>