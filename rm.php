<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style>
	
	h3
	{
	color:black;
	font-family:fantasy;
	border-style:solid;
	margin-right: 200px;
	margin-left:100px;
	border-radius: 50px;
    background: gold;
    padding: 20px; 
    width: 950px;
    height: 10px;
    text-align: center;
	}
	.page
	{
	color:black;
	font-family:fantasy;
	border-style:solid;
	margin-right: 200px;
	margin-left:100px;
	border-radius: 50px;
    background: gold;
    padding: 100px; 
    width: 800px;
    height: 300px;
    text-align: center;
	
	}



</style>
<body>
	<div align="middle">
		<img src="images/logo3.png" align="left"> 
		<h1 style="font-family: Old English Text MT; color: green;">St. Paul University Philippines</h1>
		<p>Tuguegarao City</p> <br>
	</div>
<hr>
<br>
	<h3 align="center">SPUP’S PVCD REAPS AWARDS IN UNESCO CLUBS INTERNATIONAL ASSEMBLY</h3>
<br><hr>


<div class="page">

<p><i>2017-09-03</i></p>

	<p>
		(by: Ms. Noemi Cabaddu and Dr. Allan Peejay Lappay) 
<br>
<br>
		The Paulinian Volunteers for Community Development (PVCD) Club was honoured as OUTSTANDING UNESCO CLUB (Education Category) and obtained a Certificate of GOOD STANDING during the annual assembly of UNESCO Clubs in the Philippines. Moreover, Mrs. Noemi Cabaddu (CES Director and PVCD Adviser) was awarded OUTSTANDING UNESCO CLUB EDUCATOR; while, Ms. Cheeni Mabbayad (PVCD President) was hailed as OUTSTANDING UNESCO CLUB YOUTH LEADER. The awards were presented during the 2017 International Assembly of Youth for UNESCO held at the Icon Hotel, Quezon City on September 01-03, 2017.
<br>
<br>
		The awarding served as one of the highlights of this year’s conference organized by the National Association of UNESCO Clubs in the Philippines (NAUCP), which was participated in by more than 300 participants from the country and abroad. Mr. Jonathan Guerero (President, NAUCP) and Dr. Serafin Arviola, Jr. (Chairman, NAUCP) led the recognition of the awardees.
<br>
<br>
		(SPUP's PVCD is the FIRST UNESCO-Recognized Student CLUB in REGION II and the ONLY in the St. Paul University System. It is also the ONLY RECIPIENT from the Cagayan Valley Region of this year's Outstanding award.)
	</p>

</div>

</body>
</html>