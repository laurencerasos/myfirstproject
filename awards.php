<!DOCTYPE html>
<html>
<head>
	<title>St. Paul University Philippines - Awards and Citations</title>
	<!-- TODO Bootstrap dependencies  -->
	<!-- Dan Mikko Mazo did this! -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">


</head>
<body>
<div class="container-fluid bgcolormain">
	<div class="container">
		<div class="row headerColor">
			<div class="col-md-1">
				<img src="images/logo3.png" style="padding-top: 13px;">				
			</div> 
			<div class="col-md-4">
				<h3 class="stpaul">St. Paul University Philippines</h3>
				<p>Tuguegarao City, Cagayan 3500</p>
			</div>
			<div class="col-md-offset-4 col-md-3">
				<h3 class="motto">Caritas Veritas Scientia</h3>
			</div>
		</div>
		<div class="row">
			<nav class="navbar">
			  <div class="navclr container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a class="navbar-brand" href="#">Navigation</a>
			    </div>

			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="navclr collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav">
			        <li><a href="index.php" class="nvlink">Home <span class="sr-only">(current)</span></a></li>
			        <li><a href="history.php" class="nvlink">History <span class="sr-only">(current)</span></a></li>
			        <li><a href="visionmission.php" class="nvlink">Vision and Mission <span class="sr-only">(current)</span></a></li>
			        <li><a href="corevalues.php" class="nvlink">Core Values <span class="sr-only">(current)</span></a></li>
			        <li><a href="awards.php" class="nvlink">Awards and Citations <span class="sr-only">(current)</span></a></li>
			      </ul>
			    </div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav>
		</div>
		<!-- Note: This is the body of the project -->
		<div class="row">
			<div class="jumbotron jumbocss">
				<h1>Awards and Citations</h1>
				<ul class="awardslist">
					<li>Autonomous Institution by the Commission on Higher Education</li>
					<li>First Catholic University in Region 2</li>
					<li>First ISO 9001 Certified Catholic University and Private University in Asia</li>
					<li>The only accredited school by Asian Association of Schools of Business International (AASBI) in the Philippines</li>
					<li>One of the 12 participating schools in the Philippines for Asian International Mobility for Students (AIMS)</li>
					<li>College and Basic Education Programs accredited by Philippine Accrediting Association of Schools, Colleges and Universities (PAASCU)</li>
					<li>Graduate School Programs accredited by the Philippine Association of Colleges and Universities – Commission on Accreditation (PACUCOA)</li>
					<li>Center of Excellence in Nursing</li>
					<li>Center of Excellence in Teacher Education</li>
					<li>Center of Development in Information Technology</li>
					<li>Center of Development in Business education</li>
					<li>Expanded Tertiary Education Equivalency and Accreditation Program (ETEEAP) Participating Institution</li>
					<li>Center for Teacher Training Institution</li>
					<li>One of only 11 Universities in the Philippines granted a Knowledge for <li>Development Center by World Bank</li>
					<li>Most Outstanding Student Services of the Philippines</li>
					<li>Catholic Cultural Center by the Catholic Bishops’ Conference of the Philippines</li>
					<li>Historical Site by the National Historical Commission of the Philippines</li>
					<li>Most Environment-Friendly and Sustainable School in Region II</li>
					<li>Producer of Winners in  various searches for Outstanding Students and Professionals</li>
					<li>Producer of Board Topnotchers and Passers in Licensure Examinations</li>
					<li>UNESCO Associated Schools Project Netword (ASPnet) Affiliate</li>
				</ul>
			</div>
		</div>
		<!-- Begin Footer -->
		<div class="row headerColor">
			<div class="col-md-6">
				<h3>St. Paul University Philippines</h3>
				<p>Mabini Street,	Tuguegarao City, Cagayan, 3500 Philippines</p>
			</div>
		</div>	
		<!-- End Footer -->
	</div>
</div>
	<!-- PLACED JAVASCRIPT FILES IN BOTTOM OF PAGE, BECAUSE I DECIDED IT SO... -->
	<!-- FITE ME -->
	<!-- Scripts -->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>

</body>
</html>