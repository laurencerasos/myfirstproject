<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style>
h1 {
	color:green;
	text-align: center;
	font-family: Old English Text MT;
	}
h3{
	color:black;
	font-family:fantasy;
	border-style:solid;
	margin-right: 200px;
	margin-left:100px;
	 border-radius: 50px;
    background: gold;
    padding: 100px; 
    width: 900px;
    height: 300px; 
  }
</style>
<body>
		<h1> Paulinian Core Values </h1>
	
			
<h3>-Christ-Centeredness. Christ is the center of Paulinian life; he/she follows and imitates Christ, doing everything in reference to Him. <br>

-Commission. The Paulinian has a mission - a life purpose to spread the Good News; like Christ, he/she actively works "to save" this world, to make it better place to live in.<br>

-Community. The Paulinian is a responsible family member and citizen, concerned with building communities, promotion of people, justice, and peace, and the protection of the environment.<br>

-Charism. The Paulinian develops his/her gifts/talents to be put in the service of the community, he/she strives to grow and improve daily, always seeking the better and finer things, and the final good.<br>

-Charity. Urged on by the love of Christ, the Paulinian is warm, hospitable, and "all to all", especially to the underprivileged.<br>

 Thus, Paulinian Education is committed to the formation of self-directed Catholic Filipino men and women who find fulfillment in intelligent fellowship and responsible leadership in meeting their responsibilities to God, country, and fellowmen. </h3>
</body>
</html>